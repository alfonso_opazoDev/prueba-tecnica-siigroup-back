package org.siigroup.api_login.token.application;

import org.siigroup.api_login.user.domain.User;

public interface JwtServices {

    String getToken(User user);

}
