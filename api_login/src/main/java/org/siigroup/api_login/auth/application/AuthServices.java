package org.siigroup.api_login.auth.application;

import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.domain.User;

public interface AuthServices {
    RegisterResponseDTO login();
    RegisterResponseDTO register(User user);
}
