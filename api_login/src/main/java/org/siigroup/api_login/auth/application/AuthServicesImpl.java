package org.siigroup.api_login.auth.application;

import lombok.RequiredArgsConstructor;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.application.UserServices;
import org.siigroup.api_login.user.application.UserServicesImpl;
import org.siigroup.api_login.user.domain.User;
import org.springframework.stereotype.Service;

@Service("AuthServicesImpl")
@RequiredArgsConstructor
public class AuthServicesImpl implements AuthServices {

    private final UserServices userServices;

    public RegisterResponseDTO login() {
        return null;
    }

    public RegisterResponseDTO register(User user) {
        return userServices.save(user);
    }

}
