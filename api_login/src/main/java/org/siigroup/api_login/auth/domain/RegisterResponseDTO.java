package org.siigroup.api_login.auth.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.siigroup.api_login.token.domain.Token;
import org.siigroup.api_login.user.domain.User;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class RegisterResponseDTO {
    public Token token;
}
