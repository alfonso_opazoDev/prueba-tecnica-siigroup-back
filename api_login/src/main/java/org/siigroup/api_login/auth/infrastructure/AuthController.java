package org.siigroup.api_login.auth.infrastructure;

import lombok.RequiredArgsConstructor;
import org.siigroup.api_login.auth.application.AuthServices;
import org.siigroup.api_login.auth.domain.LoginRequestDTO;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.token.application.JwtServices;
import org.siigroup.api_login.user.domain.User;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/auth")
@RequiredArgsConstructor
public class AuthController {

    private final AuthServices authServices;
    private final JwtServices jwtServices;

    @PostMapping(value = "/login")
    public ResponseEntity<RegisterResponseDTO> login(@RequestBody LoginRequestDTO loginRequestDTO){
        return ResponseEntity.ok(authServices.login());
    }

    @PostMapping(value = "/register")
    public ResponseEntity<RegisterResponseDTO> register(@RequestBody User user){
        return ResponseEntity.ok(authServices.register(user));
    }

}
