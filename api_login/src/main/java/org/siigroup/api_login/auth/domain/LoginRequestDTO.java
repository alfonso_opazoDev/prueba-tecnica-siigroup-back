package org.siigroup.api_login.auth.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data // Crea los getters y setters
@Builder // Permite instanciar los objetos de forma limpia
@AllArgsConstructor // Relacionado a los constructures
@NoArgsConstructor
public class LoginRequestDTO {
    public String user;
    public String pass;

}
