package org.siigroup.api_login.user.application;

import org.apache.ibatis.annotations.*;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.domain.User;

import java.util.List;

public interface UserServices {
    List<User> findAll();
    User findById(@Param("id") String id);
    int deleteById(@Param("id") String id);
    RegisterResponseDTO save(User user);
    int update(User item);
    int existId(String id);
}
