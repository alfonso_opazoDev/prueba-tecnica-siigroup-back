package org.siigroup.api_login.user.infrastructure;

import lombok.RequiredArgsConstructor;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.application.UserServices;
import org.siigroup.api_login.user.domain.User;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/api/v1")
@RequiredArgsConstructor
public class UserController {

    private final UserServices userServices;

    @GetMapping
    public List<User> findAll(){
        return userServices.findAll();
    }

    @GetMapping(value = "/{id}")
    public User findById(@PathVariable("id") String id){
        return userServices.findById(id);
    }

    @PostMapping(value = "/save")
    public RegisterResponseDTO save(@RequestBody User user){
        if (user.getId().equalsIgnoreCase("")) {
            return userServices.save(user);
        }
        return null;
    }

    @DeleteMapping(value = "/{id}")
    public int deleteById(@PathVariable("id") String id){
        return userServices.deleteById(id);
    }

}
