package org.siigroup.api_login.user.domain;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.UUID;

@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
public class User {
    public String id;
    public String firstName;
    public String lastName;
    public String email;
    public String password;
    public String rut;
    public String token;
    public Role[] role;
}
