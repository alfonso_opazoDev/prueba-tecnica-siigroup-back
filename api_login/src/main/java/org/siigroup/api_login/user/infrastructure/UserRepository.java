package org.siigroup.api_login.user.infrastructure;

import org.apache.ibatis.annotations.*;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.domain.User;

import java.util.List;

@Mapper
public interface UserRepository {

    @Select("Select * From Users")
    List<User> findAll();

    @Select("Select * From Users Where id = #{id}")
    User findById(@Param("id") String id);

    @Delete("Delete From Users Where id = #{id}")
    int deleteById(@Param("id") String id);

    @Insert("INSERT INTO Users(id, firstName, lastName, email, password, rut) Values (#{id},#{firstName},#{lastName},#{email}, #{password}, #{rut}) limit 100")
    RegisterResponseDTO save(User user);

    @Update("UPDATE Users SET  firstName=#{firstName}, lastName=#{lastName}, email=#{email}, password=#{password}, rut=#{rut} Where id=#{id}")
    int update(User item);

    @Select("Select count(u.id) From Users u")
    int existId (String id);


}
