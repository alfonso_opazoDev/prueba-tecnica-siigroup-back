package org.siigroup.api_login.user.application;

import lombok.RequiredArgsConstructor;
import org.siigroup.api_login.auth.domain.RegisterResponseDTO;
import org.siigroup.api_login.user.domain.User;
import org.siigroup.api_login.user.infrastructure.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.UUID;

@Service("UserServicesImpl")
@RequiredArgsConstructor
public class UserServicesImpl implements UserServices {

    @Autowired
    private final UserRepository userRepository;

    @Override
    public List<User> findAll() {
        return userRepository.findAll();
    }

    @Override
    public User findById(String id) {
        return userRepository.findById(id);
    }

    @Override
    public int deleteById(String id) {
        return userRepository.deleteById(id);
    }

    @Override
    public RegisterResponseDTO save(User user) {
        if (user.getId().equalsIgnoreCase("")) user.setId(String.valueOf(UUID.randomUUID()));
        return userRepository.save(user);
    }

    @Override
    public int update(User user) {
        return userRepository.update(user);
    }

    @Override
    public int existId(String id) {
        return userRepository.existId(id);
    }


}
