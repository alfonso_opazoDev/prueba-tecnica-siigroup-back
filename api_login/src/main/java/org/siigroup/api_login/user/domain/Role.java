package org.siigroup.api_login.user.domain;

public enum Role {
    ADMIN,
    USER,
}
