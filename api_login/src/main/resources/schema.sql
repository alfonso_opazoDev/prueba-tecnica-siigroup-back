drop table Users;

create table if not exists Users(
    id varchar(100) primary key,
    firstName varchar(100) not null,
    lastName varchar(100) not null,
    email varchar(100),
    password varchar(100) not null,
    rut varchar(100) not null
    );

create table if not exists Token(
    tokenId varchar primary key unique,
    token varchar(100) not null unique,
    expirationDate date,
    generationDate date
);